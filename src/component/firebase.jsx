import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth"
const firebaseConfig = {
    apiKey: "AIzaSyC3qZrmi84a6OwWfeX2bdrpX9Ye4bJ32p0",
    authDomain: "nakshatra2k23.firebaseapp.com",
    projectId: "nakshatra2k23",
    storageBucket: "nakshatra2k23.appspot.com",
    messagingSenderId: "78480401666",
    appId: "1:78480401666:web:ad3019e6162e9ff9c0ce1b",
    measurementId: "G-KM10B2QYEE"
  };
 const fireDb=initializeApp(firebaseConfig);
 export const auth=getAuth(fireDb);
 