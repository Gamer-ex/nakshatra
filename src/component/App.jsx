import React from "react";
import Navbar from "./Navbar";
import Login from "./Login";
function App()
{
    return(<div>
    <Navbar />
    <Login />
    </div>
    )
}
export default App;