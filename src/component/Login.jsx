
import React, { useState } from 'react'
import { createUserWithEmailAndPassword,signInWithEmailAndPassword,
  onAuthStateChanged,
  signOut } from 'firebase/auth';
import { auth } from "./firebase";
import "firebase/auth";
import "./Login.css"


function Login
() {
  const [registerEmail, setRegisterEmail] = useState("");
  const [registerPassword, setRegisterPassword] = useState("");
  const [loginEmail, setLoginEmail] = useState("");
  const [loginPassword, setLoginPassword] = useState("");

  const [user, setUser] = useState({});

  onAuthStateChanged(auth, (currentUser) => {
    setUser(currentUser);
  });

  const register = async () => {
    try {
      const user = await createUserWithEmailAndPassword(
        auth,
        registerEmail,
        registerPassword
      );
      console.log(user);
    } catch (error) {
      console.log(error.message);
    }
  };

  const login = async () => {
    try {
      const user = await signInWithEmailAndPassword(
        auth,
        loginEmail,
        loginPassword
      );
      console.log(user);
    } catch (error) {
      console.log(error.message);
    }
  };

  const logout = async () => {
    await signOut(auth);

  };

  return (
    <body>

    <div class="main">  	
		<input type="checkbox" id="chk" aria-hidden="true" />

			<div class="signup">
				<form>
					<label for="chk" aria-hidden="true">Sign up</label>
					<input type="email" name="email" placeholder="Email" required=""  onChange={(event) => {
            setRegisterEmail(event.target.value);
          }} />
					<input type="password" name="pswd" placeholder="Password" required="" onChange={(event) => {
            setRegisterPassword(event.target.value);
          }} /> 

					<button onClick={register}>Sign up</button>
				</form>
			</div>

			<div class="login">
				<form>
					<label for="chk" aria-hidden="true">Login</label>
					<input type="email" name="email" placeholder="Email" required="" onChange={(event) => {
            setLoginEmail(event.target.value);
          }} />
					<input type="password" name="pswd" placeholder="Password" required=""  onChange={(event) => {
            setLoginPassword(event.target.value);
          }}/>
					<button onClick={login}>Login</button>
				</form>
        <h4> User Logged In: </h4>
        {user?.email}
    
        <button onClick={logout}> Sign Out </button>
        
      </div> 
    </div> 
			
    </body>

	
  );
}


export default Login;